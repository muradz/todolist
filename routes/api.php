<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/user', function(Request $request) {
//    File::put('mylog.txt', $request->user());
//    return $request->user();
//})->middleware('auth:api');

Route::get('/user', function (Request $request){
    File::put('mylog.txt', $request->user());
    return $request->user();
})->middleware('auth:api');

Route::post("/register", "UserController@register");

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource("/tasks", "TaskController");
    Route::resource("/types", "TypeController");
    Route::resource("/priorities", "PriorityController");
    Route::get("/tasksByType/{typeID}/{userID}", "TaskController@getTasksByType");
    Route::get("/trashTask/{code}", "TaskController@addToTrash");

});

