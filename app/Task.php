<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title', 'type_id', 'priority_id', 'status_id', 'due_date', 'comment', 'completed', 'user_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
