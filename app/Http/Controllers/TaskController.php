<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;
use Exception;

class TaskController extends
Controller
{
    public function index(Request $request) {
        return Task::all()->where('status_id', 1)->where('completed', 0)->where('user_id', $request->userID);
    }

    public function store(Request $request) {
           // File::put('mylog.txt', $request);
        try {
            Task::create($request->all());
            return 1;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    // changes the task to completed one
    public function edit($id) {
        try {
            $task = Task::find($id);
            $task->completed = true;
            $task->save();
            return 1;
        } catch (Exception $ex) {
            return 0;
        }

    }

    // gets tasks by typeID
    public function getTasksByType($typeID, $userID){
        $tasks = Task::where('type_id', $typeID)->where('status_id', 1)->where('completed', 0)->where('user_id', $userID)->get();
        return $tasks;
    }

    // delete first time
    public function addToTrash($id) {
        try {
            $task = Task::find($id);
            $task->status_id = 2;
            $task->save();
            return 1;
        } catch (Exception $ex) {
            return 0;
        }
    }
}